let weekNumber = require("./weekNumber");
let moment = require('moment');
let origin = require('./origin');

test('moment', () => {
    expect(moment(new Date(2011, 10, 19)).diff(origin, 'day')).toBe(2);
    expect(moment(new Date(2011, 10, 25)).diff(origin, 'day')).toBe(8);
    expect(moment(new Date(2011, 11, 2)).diff(origin, 'day')).toBe(15);
});

test('week number', () => {
    expect(weekNumber(new Date(2011, 10, 19))).toBe(0);
    expect(weekNumber(new Date(2011, 10, 20))).toBe(0);
    expect(weekNumber(new Date(2011, 10, 23))).toBe(0);
    expect(weekNumber(new Date(2011, 10, 24))).toBe(1);
    expect(weekNumber(new Date(2011, 10, 30))).toBe(1);
    expect(weekNumber(new Date(2011, 11, 1))).toBe(2);
    expect(weekNumber(new Date(2011, 11, 7))).toBe(2);
    expect(weekNumber(new Date(2011, 11, 8))).toBe(3);
    expect(weekNumber(new Date(2011, 11, 14))).toBe(3);

    const w1 = weekNumber(new Date(2019, 10, 27));
    const w2 = weekNumber(new Date(2019, 10, 28));
    expect(w2 - w1).toBe(1);
});

test('backward compatibility test', () => {
    date = moment('2019/11/10', 'YYYY/MM/DD');
    let result = weekNumber(date);
    expect(result).toBe(416);
});

test('first deploy week test', () => {
    date = moment('2019/11/08', 'YYYY/MM/DD');
    let result = weekNumber(date);
    expect(result).toBe(416);
});

test('first deploy week test', () => {
    date = moment('2019/11/06', 'YYYY/MM/DD');
    let result = weekNumber(date);
    expect(result).toBe(415);
});