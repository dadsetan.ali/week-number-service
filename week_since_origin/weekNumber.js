let origin = require('./origin');
let moment = require('moment');

function WeekNumber(date) {
    return Math.floor((moment(date).diff(origin, 'day')) / 7);
}

module.exports = WeekNumber;