var express = require('express');
var router = express.Router();
let moment = require('moment');
let weekNumber = require('../week_since_origin');

/* GET home page. */
router.post('/', function (req, res, next) {
    let { dateString } = req.body;
    try {
        let date = moment(dateString, 'YYYY/MM/DD');
        let result = weekNumber(date);
        console.log('result', result);
        res.status(200).send(result.toString());
    } catch (err) {
        console.log(err, 'err');
        res.sendStatus(500).send(err);
    }
});

module.exports = router;
